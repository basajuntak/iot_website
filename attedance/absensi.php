<!DOCTYPE html>
<html>
<head>
	<?php include "header.php"; ?>
	<title>Rekapitulasi Absensi</title>
	<!-- scanning membaca kartu RFID -->
	<script type="text/javascript">
		$(document).ready(function() {
			setInterval(function(){
				$("#cekkartu").load('rekap.php')
			}, 2000);
		});	
	</script>
</head>
<body onload=display_ct();>

	<?php include "menu.php"; ?>

	<!-- isi -->
	<div class="container-fluid">
		<span id='ct5' style="padding: 10px;" class="btn btn-success"></span><br><br>
		<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">
		
		
		<table class="table table-bordered" id="myTable">
			<thead>
				<tr style="background-color: grey; color:white">
					<th style="width: 10px; text-align: center">No.</th>
					<th style="text-align: center">Nama</th>
					<th style="text-align: center">Tanggal</th>
					<th style="text-align: center">Jam Masuk</th>
					<th style="text-align: center">Jam Istirahat</th>
					<th style="text-align: center">Jam Kembali</th>
					<th style="text-align: center">Jam Pulang</th>
					<th style="text-align: center">Jam Kerja</th>
					<th style="text-align: center">Edit</th>
				</tr>
			</thead>
			<tbody>
				<?php
					include "koneksi.php";

					//baca tabel absensi dan relasikan dengan tabel karyawan berdasarkan nomor kartu RFID untuk tanggal hari ini

					//baca tanggal saat ini
					date_default_timezone_set('Asia/Jakarta');
					$tanggal = date('Y-m-d');

					//filter absensi berdasarkan tanggal saat ini
					$sql = mysqli_query($konek, "select b.nama, a.id, a.nokartu,  a.tanggal, a.jam_masuk, a.jam_istirahat, a.jam_kembali, a.jam_pulang from absensi a, karyawan b where a.nokartu=b.nokartu and a.tanggal='$tanggal' order by b.nama;");

					$no = 0;
					while($data = mysqli_fetch_array($sql))
					{
						$nokartu = $data['nokartu'];
						$no++;
				?>
				<tr>
					<td> <?php echo $no; ?> </td>
					<td> <a href="tampil_rekap.php?id=<?php echo $data['nama']; ?>"><b><?php echo $data['nama']; ?></b></a> </td>
					<td> <?php echo $data['tanggal']; ?> </td>
					<td> 
					<?php 
							if($data['jam_masuk'] == NULL){
								$status = 1;
						?>	
							<div id="cekkartu"></div>
							<button class="btn btn-success btn-block"><i class="fa fa-check"></i> Masuk</button>
							
						<?php } 
							else{
								echo $data['jam_masuk'];
							}
						?>
					</td>
					<td> 
					<?php 
							if(!($data['jam_masuk'] == NULL)){
								if($data['jam_istirahat'] == NULL){
									$status = 2;
						?>	
							<div id="cekkartu"></div>
							<button class="btn btn-success btn-block"><i class="fa fa-check"></i> Istirahat</button>
							
							
						<?php } 
							else{
								echo $data['jam_istirahat'];
							}
						}
						?>
					</td>
					<td> 
						<?php 
							if(!($data['jam_istirahat'] == NULL)){
								if($data['jam_kembali'] == NULL){
									$status = 3;
						?>	
							<div id="cekkartu"></div>
							<button class="btn btn-success btn-block"><i class="fa fa-check"></i> Kembali</button>
							
						<?php } 
							else{
								echo $data['jam_kembali'];
							}
						}
						?>
					</td>
					<td> 
					<?php 
							if(!($data['jam_kembali'] == NULL)){
								if($data['jam_pulang'] == NULL){
									$status = 4;
						?>	
							<div id="cekkartu"></div>
							<button class="btn btn-success btn-block"><i class="fa fa-check"></i> Pulang</button>
							
						<?php } 
							else{
								$status = 5;
								echo $data['jam_pulang'];
							}
						}
						?>
					</td>
					<td>
						<?php
						$date1=date_create($data['jam_masuk']);
						$date2=date_create($data['jam_pulang']);
						$diff1=date_diff($date1,$date2);
						if(!($data['jam_pulang'] == NULL)){
							echo $diff1->format("%H:%I:%S");
						}
						mysqli_query($konek, "update absensi set status='$status' where tanggal='$tanggal' and nokartu = '$nokartu'");
						?>	
							
					</td>
					<td>
					<a class = "btn btn-xs btn-info btn-block" href="editrekap.php?id=<?php echo $data['id']; ?>"> Edit</a>					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>

	<?php include "footer.php"; ?>
	<script>
		document.getElementById('ct5').style.fontSize = "x-large";
		
		function display_ct5() {
			var x = new Date()

			// date part ///
			var month=x.getMonth()+1;
			var day=x.getDate();
			var year=x.getFullYear();
			if (month <10 ){month='0' + month;}
			if (day <10 ){day='0' + day;}
			var x3= ', ' + year +'-'+month+'-'+day;

			//get day
			var weekday = new Array(7);
			weekday[0] = "Sunday";
			weekday[1] = "Monday";
			weekday[2] = "Tuesday";
			weekday[3] = "Wednesday";
			weekday[4] = "Thursday";
			weekday[5] = "Friday";
			weekday[6] = "Saturday";

			var n = weekday[x.getDay()];
			var x3 = n + x3;

			// time part //
			var hour=x.getHours();
			var minute=x.getMinutes();
			var second=x.getSeconds();
			if(hour <10 ){hour='0'+hour;}
			if(minute <10 ) {minute='0' + minute; }
			if(second<10){second='0' + second;}
			var x3 = "Rekap Absensi  : " + x3 + ' ' +  hour+':'+minute+':'+second
			document.getElementById('ct5').innerHTML = x3;
			display_c5();
		}
		function display_c5(){
			var refresh=1000; // Refresh rate in milli seconds
			mytime=setTimeout('display_ct5()',refresh)
		}
		display_c5()
	</script>
	<script>
		function myFunction() {
		var input, filter, table, tr, td, i, txtValue;
		input = document.getElementById("myInput");
		filter = input.value.toUpperCase();
		table = document.getElementById("myTable");
		tr = table.getElementsByTagName("tr");
		for (i = 0; i < tr.length; i++) {
			td = tr[i].getElementsByTagName("td")[2];
			if (td) {
			txtValue = td.textContent || td.innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				tr[i].style.display = "";
			} else {
				tr[i].style.display = "none";
			}
			}       
		}
		}
	</script>
</body>
</html>