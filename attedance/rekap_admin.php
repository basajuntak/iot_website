<?php 
	include "koneksi.php";

    $simpan = false;
    //baca tabel tmprfid
	$baca_kartu = mysqli_query($konek, "select * from tmprfid");
	$data_kartu = mysqli_fetch_array($baca_kartu);
	$nokartu    = $data_kartu['nokartu'];
    
	//tanggal dan jam hari ini
	date_default_timezone_set('Asia/Jakarta') ;
	$tanggal = date('Y-m-d');
	$jam     = date('H:i:s');

	//baca tabel status untuk mode absensi
    
	$sql = mysqli_query($konek, "select * from absensi where nokartu='$nokartu' and tanggal='$tanggal'");
	$data = mysqli_fetch_array($sql);
	$mode_absen = $data['status'];

	//uji mode absen
	$mode = "";
	if($mode_absen==1)
		$mode = "Masuk";
	else if($mode_absen==2)
		$mode = "Istirahat";
	else if($mode_absen==3)
		$mode = "Kembali";
	else if($mode_absen==4)
		$mode = "Pulang";
	
	

	//cek nomor kartu RFID tersebut apakah terdaftar di tabel karyawan
	$cari_karyawan = mysqli_query($konek, "select * from karyawan where nokartu='$nokartu'");
	$jumlah_data = mysqli_num_rows($cari_karyawan);

	if($jumlah_data==0)
		echo "";
	else
	{
		//ambil nama karyawan
		$data_karyawan = mysqli_fetch_array($cari_karyawan);
		$nama = $data_karyawan['nama'];

		

		//cek di tabel absensi, apakah nomor kartu tersebut sudah ada sesuai tanggal saat ini. Apabila belum ada, maka dianggap absen masuk, tapi kalau sudah ada, maka update data sesuai mode absensi
		$cari_absen = mysqli_query($konek, "select * from absensi where nokartu='$nokartu' and tanggal='$tanggal'");
		//hitung jumlah datanya
		$jumlah_absen = mysqli_num_rows($cari_absen);
		if($jumlah_absen == 0)
		{
			echo "<h1>Selamat Datang <br> $nama</h1>";
			$simpan = mysqli_query($konek, "insert into absensi(nokartu, tanggal)values('$nokartu', '$tanggal')");
		}
		else
		{
			//update sesuai pilihan mode absen
			if($mode_absen == 1)
			{
				$simpan = mysqli_query($konek, "update absensi set jam_masuk='$jam' where nokartu='$nokartu' and tanggal='$tanggal'");
			}
			if($mode_absen == 2)
			{
				$simpan = mysqli_query($konek, "update absensi set jam_istirahat='$jam' where nokartu='$nokartu' and tanggal='$tanggal'");
			}
			else if($mode_absen == 3)
			{
				$simpan = mysqli_query($konek, "update absensi set jam_kembali='$jam' where nokartu='$nokartu' and tanggal='$tanggal'");
			}
			else if($mode_absen == 4)
			{
				$simpan = mysqli_query($konek, "update absensi set jam_pulang='$jam' where nokartu='$nokartu' and tanggal='$tanggal'");
			}
		}
	}
	
	//jika berhasil tersimpan, tampilkan pesan Tersimpan,
	//kembali ke data karyawan
	if($simpan)
	{
		
		echo "
			<script>
				alert('Selamat $mode  $nama');
				location.replace('absensi_admin.php');
			</script>
		";
	}
	//kosongkan tabel tmprfid
	mysqli_query($konek, "delete from tmprfid");
	

?>