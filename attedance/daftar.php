<?php 
$host_db="localhost";
$user_db="root";
$pass_db="";
$nama_db="db_absen";

$mysqli = new mysqli($host_db,$user_db,$pass_db,$nama_db);

if(isset($_POST['daftar'])){
    $noKartu = $mysqli->real_escape_string($_POST['noKartu']);
	$nama = $mysqli->real_escape_string($_POST['nama']);
	$alamat = $mysqli->real_escape_string($_POST['alamat']);
	$email = $mysqli->real_escape_string($_POST['email']);
	$password = $mysqli->real_escape_string($_POST['password']);
	$passwd_hash = password_hash($password, PASSWORD_DEFAULT); // hash password

	$add_data = $mysqli->prepare("insert into admin(noKartu,nama,alamat,email,passwd)values(?,?,?,?,?)");
	$add_data ->bind_param("sssss",$noKartu,$nama,$alamat,$email,$passwd_hash);
	if($add_data ->execute()){
		echo "Berhasil";
	}else{
		echo "Gagal";
	}

	$mysqli->close();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Form Daftar</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<h1>Daftar Member </h1>
	<form action="" method="post">
		<table border="0">
			<tbody>
                <tr>
					<td>Nomer Kartu</td>
					<td><input type="text" name="noKartu"></td>
				</tr>
				<tr>
					<td>Nama</td>
					<td><input type="text" name="nama"></td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td><textarea name="alamat"></textarea></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><input type="text" name="email"></td>
				</tr>			
				<tr>
					<td>Password</td>
					<td><input type="password" name="password"></td>
				</tr>						
				<tr>
					<td>&nbsp;</td>
					<td><input type="submit" name="daftar" value="Daftar"></td>
				</tr>										
			</tbody>
		</table>
	</form>
</body>
</html>