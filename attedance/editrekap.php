<!-- proses penyimpanan -->

<?php 
	include "koneksi.php";

	//baca ID data yang akan di edit
	$id = $_GET['id'];

	//baca data karyawan berdasarkan id
	$cari = mysqli_query($konek, "select * from absensi where id ='$id'");
	$hasil = mysqli_fetch_array($cari);

	$nokartu = $hasil['nokartu'];
	$data = mysqli_query($konek, "select b.nama, a.nokartu from absensi a, karyawan b where b.nokartu = '$nokartu' and a.id ='$id'");
	$data_nama = mysqli_fetch_array($data);

	//jika tombol simpan diklik
	if(isset($_POST['btnSimpan']))
	{
		//baca isi inputan form
		$nama = $_POST['nama'];
		$jam_masuk = $_POST['jam_masuk'];
		$jam_istirahat = $_POST['jam_istirahat'];
		$jam_kembali  = $_POST['jam_kembali'];
        $jam_pulang  = $_POST['jam_pulang'];

		if(!($_POST["jam_masuk"] == NULL)){
			//simpan ke tabel karyawan
            $simpan = mysqli_query($konek, "update absensi set jam_masuk='$jam_masuk' where id='$id'");
        }
		if(!($_POST["jam_istirahat"] == NULL)){
			//simpan ke tabel karyawan
            $simpan = mysqli_query($konek, "update absensi set jam_istirahat='$jam_istirahat' where id='$id'");
        }
		if(!($_POST["jam_kembali"] == NULL)){
			//simpan ke tabel karyawan
            $simpan = mysqli_query($konek, "update absensi set jam_kembali='$jam_kembali' where id='$id'");
        }
        if(!($_POST["jam_pulang"] == NULL)){
			//simpan ke tabel karyawan
            $simpan = mysqli_query($konek, "update absensi set jam_pulang='$jam_pulang' where id='$id'");
        }

		//jika berhasil tersimpan, tampilkan pesan Tersimpan,
		//kembali ke data karyawan
		if($simpan)
		{
			echo "
				<script>
					alert('Tersimpan');
					location.replace('absensi.php');
				</script>
			";
		}
		else
		{
			echo "
				<script>
					alert('Gagal Tersimpan');
					location.replace('absensi.php');
				</script>
			";
		}

	}
?>

<!DOCTYPE html>
<html>
<head>
	<?php include "header.php"; ?>
	<title>Edit Rekapitulasi Absensi</title>
</head>
<body>

	<?php include "menu.php"; ?>

	<!-- isi -->
	<div class="container-fluid">
		<h3>Edit Rekapitulasi Absensi</h3>

		<!-- form input -->
		<form method="POST" >
			<div class="form-group">
				<label>Nama karyawan</label>
				<input type="text" name="nama" id="nama" placeholder="jam masuk karyawan" class="form-control" style="width: 200px" value="<?php echo $data_nama['nama']; ?>">
			</div>
			<div class="form-group">
				<label>Jam Masuk</label>
				<input type="time" name="jam_masuk" id="jam_masuk" placeholder="jam masuk karyawan" class="form-control" style="width: 200px" value="<?php echo $hasil['jam_masuk']; ?>">
			</div>
			<div class="form-group">
				<label>Jam Istirahat</label>
				<input type="time" name="jam_istirahat" id="jam_istirahat" placeholder="jam istirahat karyawan" class="form-control" style="width: 200px" value="<?php echo $hasil['jam_istirahat']; ?>">
			</div>
			<div class="form-group">
				<label>Jam Kembali</label>
				<input type="time" name="jam_kembali" id="jam_kembali" placeholder="jam kembali karyawan" class="form-control" style="width: 200px" value="<?php echo $hasil['jam_kembali']; ?>">
			</div>
            <div class="form-group">
				<label>Jam Pulang</label>
				<input type="time" name="jam_pulang" id="jam_pulang" placeholder="jam pulang karyawan" class="form-control" style="width: 200px" value="<?php echo $hasil['jam_pulang']; ?>">
			</div>

			<button class="btn btn-primary" name="btnSimpan" id="btnSimpan">Simpan</button>
		</form>
	</div>

	<?php include "footer.php"; ?>

</body>
</html>